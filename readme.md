# Data Scraper
This is a PoC purpose repository storing different kinds of Data Scraping

## Conclusion: Best way is Selenium-based

## A. Selenium Example: 
Use Selenium to Automate a headless browser by webdrive:
- Chrome by chromedriver
- firefox by GeckoDriver
- PhantomJS (headless)

There is also headless selenium in docker with chrome
`docker run --net=host selenium/standalone-chrome`

SeleniumHQ has provided many docker images for different scenarios, e.g. standalone Selenium and Selenium-grid in https://github.com/SeleniumHQ/docker-selenium
- selenium/base: Base image which includes Java runtime and Selenium Server JAR file
- selenium/hub: Image for running a Grid Hub
- selenium/node-base: Base image for Grid Nodes which includes a virtual desktop environment
- selenium/node-chrome: Grid Node with Chrome installed, needs to be connected to a Grid Hub
- selenium/node-firefox: Grid Node with Firefox installed, needs to be connected to a Grid Hub
- selenium/node-chrome-debug: Grid Node with Chrome installed and runs a VNC server, needs to be connected to a Grid Hub
- selenium/node-firefox-debug: Grid Node with Firefox installed and runs a VNC server, needs to be connected to a Grid Hub
- selenium/standalone-chrome: Selenium Standalone with Chrome installed
- selenium/standalone-firefox: Selenium Standalone with Firefox installed
- selenium/standalone-chrome-debug: Selenium Standalone with Chrome installed and runs a VNC server
- selenium/standalone-firefox-debug: Selenium Standalone with Firefox installed and runs a VNC server

## B. Scrapy (with/without Splash)
Visit subproject [ScrapyRunner](ScrapyRunner)