# Scrapy Runner project
It is a experimental project to understand How Scrapy works on web crawling, and help to understand Python

## Project Feature
- Anaconda 4.5.12
- Python 3.6.7 (managed by Anaconda)
- Scrapy 1.5.1
- scrapy-splash

## Development Environment Setup
- Do:
-- Download and Install [Anaconda](https://www.anaconda.com/download/) *It will have Python contained inself as base environment*
-- Download and Install VS Code, then setup Python extensions
-- **STRICTLY** follow the scrapy tutorial, use channel "conda-forge" to resolve the scrapy dependencies (i.e. using `conda install -c conda-forge scrapy` and set channels in [anaconda-project.yml](anaconda-project.yml)) instead of default channels. Missing this part may results in lxml/etree Dll not found exception during VS Code debug procedure.
- Don't
-- Don't try to manage python environment with Visual Studio, or install Anaconda with Visual Studio Installer. You will find it won't work at the end when you want to manage environment for release

## Use anaconda-project to initialize the project dependencies and Run the project default command
- In avaconda prompt, under the project directory, execute `anaconda-project run`

## Debugging with VS Code
- Debug the runner.py with "Scrapy" Debug Configuration in [vscode launch.json](.vscode\launch.json)
-- Breakpoint works in this way

## Use Splash dashboard to visually develop lua script (or test it)
After running the splash docker, ported on 8050, you can browse the page on http://localhost:8050 to test the result of HTML crawling with different config.

## Findings
### - Concept of Python Environment and Virtual Environment
- The concept of environment is similar to Java. Each project can have different dependency on different version of Python
- However, even the libraries are loaded as part of the environment.
- To avoid polluting the base environment, **Virtual Environment** are suggested to be created for each project.
### - Package Management Tools
- Python basic uses **pip** as package management. However, it's dependency-tracing is not good.
- **Conda** instead eases the whole package management process, even the version of Python.
-- **Anaconda** is the easiest way to include most useful packages as project base
- **Anaconda Navigator** (included in Anaconda installation) is a good tool to manage all Python environments and tools
- However, some packages do not exist on conda-forge or anaconda. These packages can only be pip-ed after the environment setup.
### - Dynamic (Javascript-loaded) content handling
- For some webpages, contents are loaded by JS instead of HTML. (e.g. HKTVMall)
- As a solution, the solution can be either Selenium+chromedriver, scrapy-splash, etc.
- **scrapy-splash** tutorial followed https://www.jianshu.com/p/8a8d0ceed8d3
-- p.s. Docker is required in the splash environment
### - Scrapy shell debugging
- In normal situation without scrapy-splash, Scrapy shell debugging can be used via `scrapy shell "https://www.hktvmall.com"`
- In scrapy-splash environment, splash must be used as the agent. So the Scrapy shell script need to be changed to `scrapy shell "http://localhost:8050/render.html?url=https://www.hktvmall.com"`, where "render.html" is just a default value of parameter "endpoint", which can be configured in "args" parameter when invoking `SplashRequest()` 

## Resources
- Follow primary Scrapy tutorial https://docs.scrapy.org/en/latest/intro/tutorial.html
- Export conda environment https://conda.io/docs/commands/env/conda-env-export.html
- Conda environment management: Create env from environment yml file 
https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file
- Anaconda user-guide http://docs.anaconda.com/anaconda/user-guide/getting-started/
- Anaconda介紹及安裝教學 https://medium.com/python4u/anaconda%E4%BB%8B%E7%B4%B9%E5%8F%8A%E5%AE%89%E8%A3%9D%E6%95%99%E5%AD%B8-f7dae6454ab6
- 用conda建立及管理python虛擬環境 https://medium.com/python4u/%E7%94%A8conda%E5%BB%BA%E7%AB%8B%E5%8F%8A%E7%AE%A1%E7%90%86python%E8%99%9B%E6%93%AC%E7%92%B0%E5%A2%83-b61fd2a76566