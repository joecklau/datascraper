import scrapy
from scrapy.crawler import CrawlerProcess

# from tutorial.spiders.hktvmall_top100 import HktvmallTop100Spider
from tutorial.spiders.fb_officialchapmanto import FbOfficialchapmantoSpider

process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    'FEED_FORMAT': 'json',
    'FEED_URI': 'data.json'
})

# process.crawl(HktvmallTop100Spider)
process.crawl(FbOfficialchapmantoSpider)
process.start() # the script will block here until the crawling is finished