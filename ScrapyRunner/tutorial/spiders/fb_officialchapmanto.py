# -*- coding: utf-8 -*-
import scrapy
import scrapy_splash
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy_splash.request import SplashRequest, SplashFormRequest

class FbOfficialchapmantoSpider(scrapy.Spider):
    name = "fbOfficialchapmantoSpider"

    # lua_script = """
    #     function main(splash, args)
    #     local ok, reason = splash:go(args.url)
    #     user_name = args.user_name
    #     user_passwd = args.user_passwd
    #     user_text = splash:select("#email")
    #     pass_text = splash:select("#pass")
    #     login_btn = splash:select("#loginbutton")
    #     if (user_text and pass_text and login_btn) then
    #         user_text:send_text(user_name)
    #         pass_text:send_text(user_passwd)
    #         login_btn:mouse_click({})
    #     end

    #     splash:wait(math.random(5, 10))
    #     return {
    #         url = splash:url(),
    #         cookies = splash:get_cookies(),
    #         headers = splash.args.headers,
    #     }
    # end
    # """

    def start_requests(self):
        url = 'https://www.facebook.com/pg/officialchapmanto/posts'
        splash_args = {"lua_source": """
                    --splash.response_body_enabled = true
                    splash.private_mode_enabled = false
                    splash:set_user_agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
                    assert(splash:go(""" + url + """))
                    splash:wait(5)
                    return {html = splash:html()}
                    """}
        # yield SplashRequest(url, endpoint='run', args=splash_args, callback=self.parse)
        yield SplashRequest(
            url=url, 
            callback=self.parse,
            args=splash_args,
            # endpoint='render.json', # optional; default is render.html
            # splash_url='<url>',     # optional; overrides SPLASH_URL
            slot_policy=scrapy_splash.SlotPolicy.PER_DOMAIN,  # optional
        )

    def parse(self, response):
        #开启爬取之前先登录
        yield Request(
            url= self.login_url, # https://www.facebook.com/login
            callback= self.login,
        )
        yield SplashFormRequest.from_response(
            response,
            url = self.login_url,
            formdata={
                "email":user,
                "pass": password
            },
            endpoint="execute",
            args={
                "wait": 30,
                "lua_source": lua_script, #这个参数是一个lua脚本的字符串
                "user_name" : user,  #user和password将会作为参数传入到lua脚本中
                "user_passwd" : password,
            },
            callback = self.after_login,
            errback = self.error_parse,
        )

        value = response.xpath("//*[@data-testid='UFI2ReactionsCount/sentenceWithSocialContext']//text()").extract()
        print(value)

        page = response.url.split("/")[-2]
        
        filename = 'mall-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)

        for product in response.css('.trp-product'):
            yield {
                'brandproductname': product.css('.brand-product-name h4::text').extract(),
                'price': product.css('.price span::text').extract(),
                'url': product.css('.product-brief a::attr(href)').extract(),
            }