# -*- coding: utf-8 -*-
import scrapy
import scrapy_splash
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy_splash.request import SplashRequest, SplashFormRequest

class HktvmallTop100Spider(scrapy.Spider):
    name = "hktvmallTop100Spider"

    def start_requests(self):
        url = 'https://www.hktvmall.com/'
        # splash_args = {"lua_source": """
        #             --splash.response_body_enabled = true
        #             splash.private_mode_enabled = false
        #             splash:set_user_agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
        #             assert(splash:go(""" + url + """))
        #             splash:wait(3)
        #             return {html = splash:html()}
        #             """}
        # yield SplashRequest(url, endpoint='run', args=splash_args, callback=self.parse)
        yield SplashRequest(url, self.parse,
            args={
                # optional; parameters passed to Splash HTTP API
                'wait': 3,

                # 'url' is prefilled from request url
                # 'http_method' is set to 'POST' for POST requests
                # 'body' is set to request body for POST requests
            },
            # endpoint='render.json', # optional; default is render.html
            # splash_url='<url>',     # optional; overrides SPLASH_URL
            slot_policy=scrapy_splash.SlotPolicy.PER_DOMAIN,  # optional
        )

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'mall-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)

        for product in response.css('.trp-product'):
            yield {
                'brandproductname': product.css('.brand-product-name h4::text').extract(),
                'price': product.css('.price span::text').extract(),
                'url': product.css('.product-brief a::attr(href)').extract(),
            }